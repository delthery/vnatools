TEMPLATE=lib

DEFINES += NETUDP_DLL_EXPORT
DEFINES+="NETUDP_VERSION_MAJOR=2"
DEFINES+="NETUDP_VERSION_MINOR=0"
DEFINES+="NETUDP_VERSION_PATCH=2"
DEFINES+="NETUDP_VERSION_TAG_HEX=202"

#CONFIG += staticlib c++17
CONFIG += c++17
!contains( CONFIG, c\+\+1[14] ): warning("NetUdp needs at least c++11, add CONFIG += c++11 to your .pro")

INCLUDEPATH += $$PWD/..

HEADERS += \
    $$PWD/Datagram.hpp \
    $$PWD/Export.hpp \
    $$PWD/InterfacesProvider.hpp \
    $$PWD/NetUdp.hpp \
    $$PWD/Property.hpp \
    $$PWD/RecycledDatagram.hpp \
    $$PWD/Socket.hpp \
    $$PWD/Utils.hpp \
    $$PWD/Version.hpp \
    $$PWD/Worker.hpp \
    $$PWD/_Template.hpp
SOURCES += \
    $$PWD/Datagram.cpp \
    $$PWD/InterfacesProvider.cpp \
    $$PWD/RecycledDatagram.cpp \
    $$PWD/Socket.cpp \
    $$PWD/Utils.cpp \
    $$PWD/Version.cpp \
    $$PWD/Worker.cpp \
    $$PWD/_Template.cpp
