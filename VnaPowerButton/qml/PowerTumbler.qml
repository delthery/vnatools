import QtQuick 2.0
import QtQuick.Controls 2.12

Item {

    function formatText(count, modelData) {
        var data = count === 12 ? modelData + 1 : modelData;
        return data.toString().length < 2 ? "0" + data : data;
    }

    Component {
       id: delegateComponent
       Label {
           text: formatText(Tumbler.tumbler.count, modelData)
           color: "black"
           opacity: 1.0 - Math.abs(Tumbler.displacement) / (Tumbler.tumbler.visibleItemCount / 2)
           horizontalAlignment: Text.AlignHCenter
           verticalAlignment: Text.AlignVCenter
           //font.pixelSize: fontMetrics.font.pixelSize * 1.25
       }
   }

    Row {
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        Tumbler {
            model: ["+","-"]
            delegate: delegateComponent
        }
        Tumbler {
            model: 9
            delegate: delegateComponent
        }
        Tumbler {
            model: 9
            delegate: delegateComponent
        }
    }
}
