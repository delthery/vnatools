import QtQuick 2.0
import NetUdp 1.0 as NetUdp
//import QMLTcpSockets 1.0

Item {
    property alias server: server_
    property alias client: client_

    NetUdp.Socket {
        id: server_

        onDatagramReceived: function(datagram)
        {
            console.log(`datagram : ${JSON.stringify(datagram)}`)
            console.log(`datagram.data (string) : "${datagram.data}"`)
            let byteArray = []
            for(let i = 0; i < datagram.data.length; ++i)

                byteArray.push(datagram.data.charCodeAt(i))
            console.log(`datagram.data (bytes): [${byteArray}]`)

            console.log(`datagram.destinationAddress : ${datagram.destinationAddress}`)
            console.log(`datagram.destinationPort : ${datagram.destinationPort}`)
            console.log(`datagram.senderAddress : ${datagram.senderAddress}`)
            console.log(`datagram.senderPort : ${datagram.senderPort}`)
            console.log(`datagram.ttl : ${datagram.ttl}`)
        }

        Component.onCompleted: () => start()
    }

    NetUdp.Socket {
        id: client_

        onDatagramReceived: function(datagram)
        {
            console.log(`datagram : ${JSON.stringify(datagram)}`)
            console.log(`datagram.data (string) : "${datagram.data}"`)
            let byteArray = []
            for(let i = 0; i < datagram.data.length; ++i)
              byteArray.push(datagram.data.charCodeAt(i))
            console.log(`datagram.data (bytes): [${byteArray}]`)

            console.log(`datagram.destinationAddress : ${datagram.destinationAddress}`)
            console.log(`datagram.destinationPort : ${datagram.destinationPort}`)
            console.log(`datagram.senderAddress : ${datagram.senderAddress}`)
            console.log(`datagram.senderPort : ${datagram.senderPort}`)
            console.log(`datagram.ttl : ${datagram.ttl}`)
        }
        Component.onCompleted: () => start("192.168.0.103", 5025)
    }

    function getIntArr(str){
        var ret = [];
        for(var i=0;i<str.length;i++)
            ret.push(str.charCodeAt(i));
        return ret;
    }

    function getStr(intArr){
        var str = "";
        for(var i=0;i<intArr.length;i++)
            str += String.fromCharCode(intArr[i]);
        return str;
    }
    QtObject {
        id: serverPortField
        property string text: "1234"
    }

    QtObject {
        id: clientAddressField
        property string text: "192.168.0.106"
    }

    QtObject {
        id: clientPortField
        property string text: "5025"
    }
/*
    TcpServer{
        id: server

        port: parseInt(serverPortField.text)

        onNewConnection: {
            serverSocket = TcpSocketFactory.fromDescriptor(socketDescriptor);
            serverSocket.setSocketOption(TcpSocketEnums.LowDelayOption, 1);
            serverSocket.bytesReceived.connect(function(bytes){console.info("Server received: " + getStr(bytes)); });
            serverSocket.connected.connect(function(){console.info("Server socket connected."); });
            serverSocket.disconnected.connect(function(){console.info("Server socket disconnected."); });
            serverSocket.peerChanged.connect(function(){console.info("Server socket peer changed: " + serverSocket.peer); });
            serverSocket.portChanged.connect(function(){console.info("Server socket port changed: " + serverSocket.port); });
        }
    }

    property var serverSocket: null

    TcpSocket{
        id: clientSocket

        peer: clientAddressField.text
        port: parseInt(clientPortField.text)
        onBytesReceived: console.info("Client received: " + getStr(bytes))
        onConnected: {
            console.info("Client socket connected");
            setSocketOption(TcpSocketEnums.LowDelayOption, 1);
        }
        onDisconnected: console.info("Client socket disconnected")
        onPeerChanged: console.info("Client socket peer changed: " + peer)
        onPortChanged: console.info("Client socket port changed: " + port)
    }
    */
}
