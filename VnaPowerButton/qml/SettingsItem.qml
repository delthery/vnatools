import Felgo 3.0
import QtQuick 2.0
import QtQuick.Controls 2.12 as Controls

NavigationItem {
    title: "Settings"
    icon: IconType.gear
    NavigationStack {
        Page {
            title: qsTr("Settings")

            AppFlickable {
                anchors.fill: parent
                contentHeight: column.height

                Column {
                  id: column
                  width: parent.width

                  anchors.bottomMargin: 2*dp(Theme.contentPadding)

                  SimpleSection {
                    title: "Proxy"
                  }

                  Controls.ButtonGroup {
                      id: buttonGroup
                      //buttons: proxyModeButtons.children
                  }

                  AppListItem {
                      id: modeListItem
                    text: "Mode"
                    //rightText: "Longer rightText, really long"
                    rightItem: Row {
                        id: proxyModeButtons
                        anchors.verticalCenter: parent.verticalCenter
                        Controls.Button {
                            text: "Disable"
                            checkable: true
                            flat: !checked
                            Controls.ButtonGroup.group: buttonGroup
                            anchors.verticalCenter: parent.Center
                        }
                        Controls.Button {
                            text: "Auto"
                            checkable: true
                            flat: !checked
                            Controls.ButtonGroup.group: buttonGroup
                        }
                        Controls.Button {
                            text: "Manual"
                            checkable: true
                            flat: !checked
                            Controls.ButtonGroup.group: buttonGroup
                        }

                    }
                  }

                  AppListItem {
                      id: proxyListItem
                      showDisclosure: false
                      mouseArea.enabled: false
                      //text: "Proxy"
                      leftItem: Icon {
                          icon: IconType.moono
                          width: sp(26)
                          height: width
                          anchors.verticalCenter: parent.verticalCenter
                        }
                      textItem: AppTextField {
                              id: httpProxy
                              width: proxyListItem.textItemAvailableWidth
                              inputMode: inputModeUrl
                              placeholderText: "Address"
                      }
                  }

                  AppListItem {
                      showDisclosure: false
                      mouseArea.enabled: false
                      //text: "Proxy"
                      leftItem: Icon {
                          icon: IconType.digg
                          width: sp(26)
                          height: width
                          anchors.verticalCenter: parent.verticalCenter
                        }
                      textItem: AppTextField {
                          id: httpPort
                          width: proxyListItem.textItemAvailableWidth
                          inputMode: inputModeDefault
                          placeholderText: "Port"
                      }
                  }

                  AppListItem {
                      id: userListItem
                      showDisclosure: false
                      mouseArea.enabled: false
                      leftItem: Icon {
                          icon: IconType.user
                          width: sp(26)
                          height: width
                          anchors.verticalCenter: parent.verticalCenter
                        }
                      textItem: AppTextField {
                          id: login
                          width: userListItem.textItemAvailableWidth
                          inputMode: inputModeUsername
                      }
                  }

                  AppListItem {
                      leftItem: Icon {
                        icon: IconType.usersecret
                        width: sp(26)
                        height: width
                        anchors.verticalCenter: parent.verticalCenter
                      }
                      textItem: AppTextField {
                          id: password
                          width: userListItem.textItemAvailableWidth
                          inputMode: inputModePassword
                          showPasswordVisibleButton: true
                      }
                  }

                  SimpleSection {
                    title: "Other"
                  }

                  AppListItem {
                    text: "Show tray icon"
                    showDisclosure: false
                    mouseArea.enabled: false
                    rightItem: AppSwitch {
                        id: trayToggle
                        anchors.verticalCenter: parent.verticalCenter
                        checked: true
                        Component.onCompleted:
                            systemTray.visible = Qt.binding(function() { return trayToggle.checked })

                        onCheckedChanged: {
                            if(checked) systemTray.show()
                            else systemTray.hide()
                        }
                    }

                  }
                }
           }
        }
    }
}

