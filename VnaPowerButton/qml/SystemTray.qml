import QtQuick 2.0
import QtQuick.Controls 2.12
import Qt.labs.platform 1.1


SystemTrayIcon {
    property var application

    id: systemTray
    visible: false //system.isDesctopPlatform() //true
    iconSource: Qt.resolvedUrl("../assets/port.png")

    onMessageClicked: console.log("Message clicked")
    Component.onCompleted: showMessage("Message title", "Something important came up. Click this to know more.")

    onVisibleChanged: console.log("System Tray Icon is " + visible)

    menu: Menu {
        id: trayMenu
        MenuItem {
            text: "Show"
            onTriggered: application.show()
        }
        MenuItem {
            text: "Hide"
            onTriggered: application.hide()
        }
        MenuSeparator {}

        MenuItem {
            text: qsTr("Quit")
            onTriggered: Qt.quit()
        }
    }

    onActivated: {
        if(reason === 1){ //SystemTrayIcon.Context){//
            trayMenu.popup()
        } else {
            trayMenu.close()

            if(application.visibility === 0) {
                application.show()
                application.raise()
                application.requestActivate()
            } else {
                application.hide()
            }
        }
    }
}

