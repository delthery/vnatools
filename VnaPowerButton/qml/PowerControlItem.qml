import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Extras 1.4
import QtQuick.Controls 2.12 as Controls
import Felgo 3.0

NavigationItem {
    id: _root

    //property alias powerOnOff: _powerButton

    title: "Power Control"
    icon: IconType.poweroff

    NavigationStack {
        id: _stack
        property alias page: _page

        focus: true
        Keys.forwardTo: _powerButton

        Page {
            id: _page
            property alias column: _column

            title: qsTr("VNA Tools")

//            ColumnLayout {
//                width: parent.width
//                height: parent.height
//                anchors.centerIn: parent
//                spacing: dp(16)
//                anchors.bottomMargin: dp(16)
            Column {
                property alias powerButton: _powerButton

                id: _column
                width: parent.width
                height: parent.height
                //anchors.centerIn: parent
                spacing:dp(Theme.contentPadding)

                DeviceHeader {}

//                Item {
//                  Layout.fillHeight: true
//                }

                PowerTumbler {
                    width: parent.width
                }

                AppText {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text:  "<b>-10</b>dB"
                    fontSize: 24
                }

                Controls.SpinBox {
                    //decimals: 2
                    width: parent.width
                }

                DelayButton {
                    id: _powerButton
                    text: qsTr("POWER")
                    delay: 1000
                    //Layout.alignment: Qt.AlignCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                    Keys.onSpacePressed: {
                        if(event.key === Qt.Key_Space) {
                            console.log("Space pressed")
                            _powerButton.toggle()
                        }
                    }
                }

            }
        }
    }
}
