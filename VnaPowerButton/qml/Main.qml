import Felgo 3.0
import QtQuick 2.15
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12 as Layouts

App {
    id: application
    flags: Qt.Window | Qt.Dialog
    // You get free licenseKeys from https://felgo.com/licenseKey
    // With a licenseKey you can:
    //  * Publish your games & apps for the app stores
    //  * Remove the Felgo Splash Screen or set a custom one (available with the Pro Licenses)
    //  * Add plugins to monetize, analyze & improve your apps (available with the Pro Licenses)
    //licenseKey: "<generate one from https://felgo.com/licenseKey>"

//    Component.onCompleted: {
//        Theme.platform= "android"
//        Theme.colors.backgroundColor = "#000"
//        Theme.colors.secondaryBackgroundColor = "#000"
//        Theme.colors.selectedBackgroundColor = "#333"
//        Theme.colors.textColor = "#fff"
//        Theme.colors.dividerColor = "transparent"
//        Theme.listItem.backgroundColor = "#1c1c1e"
//        Theme.listItem.textColor = "#fff"
//        Theme.navigationBar.backgroundColor = Theme.colors.backgroundColor
//        Theme.navigationBar.titleColor = Theme.colors.textColor
//        Theme.navigationBar.dividerColor = "transparent"
//        Theme.colors.statusBarStyle = Theme.colors.statusBarStyleWhite
//    }

    Socket {
      id: socket
    }

    Navigation {
        id: navigation
        drawerLogoSource: Qt.resolvedUrl("../assets/planar-logo0.png")
        drawerMinifyEnabled: false
        //drawerFixed: false

        ConnectionItem {
            socket: socket
        }

        PowerControlItem {}

        NavigationItem {
            title: "Memory"
            icon: IconType.cameraretro
            NavigationStack {
                Page {
                    title: qsTr("Trace Memory")
                }
            }
        }

        SettingsItem {
        }
}

    Shortcut {
        sequence: "Ctrl+1"
        onActivated: navigation.currentIndex = 0
    }
    Shortcut {
        sequence: "Ctrl+2"
        context: Qt.ApplicationShortcut
        onActivated: navigation.currentIndex = 1
    }
    Shortcut {
        sequence: "Ctrl+3"
        context: Qt.ApplicationShortcut
        onActivated: navigation.currentIndex = 2
    }
    Shortcut {
        sequence: "Ctrl+4"
        context: Qt.ApplicationShortcut
        onActivated: navigation.currentIndex = 3
    }

    Shortcut {
        sequence: "Ctrl+0"
        context: Qt.ApplicationShortcut
        onActivated: {
            navigation.drawerMinifyEnabled != navigation.drawerMinifyEnabled
            //navigation.drawerFixed != navigation.drawerFixed
        }
    }

    SystemTray {
        id: systemTray
        application: application
    }

    onClosing: {
        /* If the check box should not be ignored and it is active,
         * then hide the application. Otherwise, close the application
         * */
        //if(checkTray.checked === true && ignoreCheck === false){
            close.accepted = false
            application.hide()
        //} else {
        //    Qt.quit()
        //}
    }

    Component.onCompleted: {
        console.log("MAIN COMPLETED")
        systemTray.visible = true
    }
}
