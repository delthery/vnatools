import QtQml 2.12
import QtQuick 2.0
import Felgo 3.0

NavigationItem {
    id: rootItem

    property var selectedItem
    required property var socket

    title: "Connection"
    icon: IconType.server
    badgeValue: "5"

    property var deviceModel: [
        { text: "ASRL1::INSTR",     icon: IconType.check,  detailText: "CMT C1220",        modelPath: "image_c1220.png"},
        { text: "ASRL2::INSTR",     icon: IconType.tablet, detailText: "CMT S5048",        modelPath: "compact_vna_preview2.png"},
        { text: "TCPIP0::INSTR",    icon: IconType.tablet, detailText: "PLANAR Obzor 304/1", modelPath: "image_obzor-304-1.png"}
    ]

    NavigationStack {
        id: navigationStack

        // Timer simulating a long running operation
        Timer {
          id: timer
          interval: 5000
          running: false
          repeat: false
          triggeredOnStart: false
          onTriggered: {
              rootItem.deviceModel.push({ text: "GPIB0::14::INSTR", icon: IconType.tablet})
              rootItem.deviceModelChanged()
//              console.log("Sending datagram to socket:" + socket)
//              socket.sendDatagram({
//                          address: "192.168.0.106",
//                          port: 5025,
//                          data: "*IDN?"
//                        })
          }
        }

        initialPage: Page {
            id: connectionPage

            title: qsTr("Connection")
            AppFlickable {
                anchors.fill: parent
                contentHeight: column.height
                Column {
                    id: column

                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent
                    spacing: dp(16)

                    DeviceHeader {
                        compact: true
                        name: selectedItem ? selectedItem.detailText : ""
                        address: selectedItem ? selectedItem.text : ""
                        serialNumber: "SN------"
                        imagePath: selectedItem ? "../assets/" + selectedItem.modelPath : ""

                        onClickedNew: {
                            console.log("Sending datagram to socket:" + socket.server)
                            socket.server.sendDatagram({
                                address: "192.168.0.106",
                                port: 5025,
                                data: "*IDN?" + String.fromCharCode(10)
                            })

                            console.log("Sending datagram to socket:" + socket.client)
                            socket.client.sendDatagram({
                                address: "192.168.0.106",
                                port: 5025,
                                data: "*IDN?" + String.fromCharCode(10)
                            })
                        }
                    }

                    AppListItem {
                        showDisclosure: false
                        mouseArea.enabled: false
                        leftItem: Icon {
                            icon: IconType.user
                            width: sp(26)
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                          }
                        text: "Model"
                        //rightText: selectedItem.detailText
                        detailText: selectedItem ? selectedItem.detailText : ""
                    }
                    AppListItem {
                        showDisclosure: false
                        mouseArea.enabled: false
                        leftItem: Icon {
                            icon: IconType.slideshare
                            width: sp(26)
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                          }
                        text: "Serial Number"
                        rightText: "SN-----" //selectedItem.serialNumber
                    }
                    AppListItem {
                        showDisclosure: false
                        mouseArea.enabled: false
                        leftItem: Icon {
                            icon: IconType.meanpath
                            width: sp(26)
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                          }
                        text: "Address"
                        rightText: selectedItem ? selectedItem.text : ""
                    }
                    AppListItem {
                        showDisclosure: false
                        mouseArea.enabled: false
                        leftItem: Icon {
                            icon: IconType.dashboard
                            width: sp(26)
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                          }
                        text: "Temperature"
                        //rightText:
                        detailText: "54 degree"//selectedItem.text
                    }


                    AppText {
                        text: "Device"
                    }

                    AppTextField {
                        anchors{
                            right: parent.right
                            left: parent.left
                        }
                    }

                    AppButton {
                        text: "Search"
                        iconLeft: IconType.search
                        onClicked: connectionPage.navigationStack.push(devicesPage)
                    }
                } // column
            }
            FloatingActionButton {
              id: searchButton
              icon: IconType.search
              visible: true // only show on Android by default
              onClicked: {
                //save()
                  connectionPage.navigationStack.push(devicesPage)
              }

              PropertyAnimation {
                target: searchButton
                property: "anchors.bottomMargin"
                duration: 500
                easing.type: Easing.InQuart
                from: -8*searchButton.anchors.bottomMargin
                to: searchButton.anchors.bottomMargin
                running: true
              }
            }

        } // page

        onPushed: timer.start()

        Component {
            id: devicesPage
            Page {                
                title: qsTr("Device list")

                rightBarItem: ActivityIndicatorBarItem {
                    visible: timer.running
                }

                AppListView {
                    delegate: SimpleRow {
                        //imageSource: item ? item.modelPath : ""
                        onSelected: {
                            rootItem.selectedItem = item
                            connectionPage.navigationStack.pop()
                            console.log("Clicked Item #" + index + ":" + JSON.stringify(modelData))
                        }
                    }
                    model: deviceModel
                } //AppListView
            } //Page
        } // Component
    } // NavigationStack
} // NavigationItem
