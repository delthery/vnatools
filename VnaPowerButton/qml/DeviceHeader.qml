import Felgo 3.0
import QtQuick 2.0
import QtQuick.Layouts 1.12

Rectangle {
    property bool compact: false
    property var address: "TCP0:INSTR"
    property var name: "TR1300"
    property var serialNumber: "SN12345678"
    property var imagePath: "../assets/compact_vna_preview2.png"

  signal clickedNew()

  width: parent.width
  height: contentRow.height + 2*dp(Theme.contentPadding)
  color: Theme.colors.tintColor
  z: 10

  RowLayout {
    id: contentRow
    width: parent.width
    anchors.centerIn: parent
    spacing: dp(Theme.contentPadding)

    Image {
      source: Qt.resolvedUrl(imagePath)
      Layout.preferredWidth: dp(150)
      Layout.preferredHeight: width
      Layout.leftMargin: dp(Theme.contentPadding)
      //radius: width/2
      fillMode: Image.PreserveAspectFit
    }

    Column {
        visible: !compact
      spacing: dp(5)

      AppText {
        text: name
        font.bold: true
        //font.pixelSize: 20
        color: "white"
      }

      AppText {
        text: serialNumber//dataModel.totalTime
        color: "white"
      }

      AppText {
        text: address//dataModel.totalTime
        color: "white"
      }

    }

    // filler
    Item {
      Layout.fillWidth: true
    }

    IconButton {
      Layout.preferredWidth: dp(50)
      Layout.preferredHeight: width
      Layout.rightMargin: dp(Theme.contentPadding)
      icon: IconType.pinterest
      size: dp(30)
      color: "white"
      selectedColor: Qt.darker(color, 1.1)
      onClicked: clickedNew()
    }
  }
}
