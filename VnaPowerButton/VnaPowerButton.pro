# allows to add DEPLOYMENTFOLDERS and links to the Felgo library and QtCreator auto-completion
CONFIG += felgo c++17

QT += widgets

# uncomment this line to add the Live Client Module and use live reloading with your custom C++ code
# for the remaining steps to build a custom Live Code Reload app see here: https://felgo.com/custom-code-reload-app/
# CONFIG += felgo-live

# Project identifier and version
# More information: https://felgo.com/doc/felgo-publishing/#project-configuration
PRODUCT_IDENTIFIER = com.planar.vna.PowerButton
PRODUCT_VERSION_NAME = 1.0.0
PRODUCT_VERSION_CODE = 1

# Optionally set a license key that is used instead of the license key from
# main.qml file (App::licenseKey for your app or GameWindow::licenseKey for your game)
# Only used for local builds and Felgo Cloud Builds (https://felgo.com/cloud-builds)
# Not used if using Felgo Live
PRODUCT_LICENSE_KEY = "FE9F4CA0E76E33B476DB429AF16BC5D69C62FF7CE2D8F19D4859B39A57D4E2165001AF3AD48E89F808EA57A7D915F319A63FC1A1071089CDAE2AD04FFA85F4AF29A815B620E4A9C358A5074C473A4DC9ADED0EDD481E8F6019BF91837223B20494014C9232778676436894FD36C58108EB24F283829C82B1A4AECCDBBE19B7F324A21D8E519096A2A9F3C9B216C8DA4F0E7C179C67B9FD02387D06E2EDFAE1DC6FE778F236E16E9BDF100F55BF2F93826F158D6FDF150A5FB57429FB3462EC1516F14D3799FBD4A0CBCA3862DE00823A01A17EACBB90098F7C418819DFF200BC1185C03B31A388F98CF0E27E95F3E726F4BCCDAFF189182954598539B9F6BFCA9AF6D254622292156B31D4BB67AC852C34A46E53574182E7BA91DAC169F6FC8FEDAC31E57E0BC91888AB0A5357E963EC"

qmlFolder.source = qml
DEPLOYMENTFOLDERS += qmlFolder # comment for publishing

assetsFolder.source = assets
DEPLOYMENTFOLDERS += assetsFolder

# Add more folders to ship with the application here

# RESOURCES += resources.qrc # uncomment for publishing

# NOTE: for PUBLISHING, perform the following steps:
# 1. comment the DEPLOYMENTFOLDERS += qmlFolder line above, to avoid shipping your qml files with the application
#    (instead they get compiled to the app binary)
# 2. uncomment the resources.qrc file inclusion and add any qml subfolders to the .qrc file;
#    this compiles your qml files and js files to the app binary and protects your source code
# 3. change the setMainQmlFile() call in main.cpp to the one starting with "qrc:/" - this loads the qml files from the resources
# for more details see the "Deployment Guides" in the Felgo Documentation

# during development, use the qmlFolder deployment because you then get shorter compilation times
# (the qml files do not need to be compiled to the binary but are just copied)
# also, for quickest deployment on Desktop disable the "Shadow Build" option in Projects/Builds -
# you can then select "Run Without Deployment" from the Build menu in Qt Creator if you only changed QML files;
# this speeds up application start, because your app is not copied & re-compiled but just re-interpreted


# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    RunGuard.cpp


android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += android/AndroidManifest.xml       android/build.gradle
}

ios {
    QMAKE_INFO_PLIST = ios/Project-Info.plist
    OTHER_FILES += $$QMAKE_INFO_PLIST
}

# set application icons for win and macx
win32 {
    RC_FILE += win/app_icon.rc
}
macx {
    ICON = macx/app_icon.icns
}

DISTFILES += \
    qml/ConnectionItem.qml \
    qml/DeviceHeader.qml \
    qml/PowerControlItem.qml \
    qml/PowerTumbler.qml

HEADERS += \
    RunGuard.h

LIBS += -L../NetUdp #-lNetUdp
LIBS += -L../qml-tcpsockets #-lqml-tcpsockets

INCLUDEPATH += ../
DEPENDPATH += ../NetUdp ../qml-tcpsocket
