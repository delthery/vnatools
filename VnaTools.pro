TEMPLATE = subdirs
 
  SUBDIRS = \
    app \
    netudp \
    qml-tcpsockets

app.subdir = VnaPowerButton
netudp.subdir = NetUdp
qml-tcpsockets = qml-tcpsockets

app.depends = netudp qml-tcpsockets
